This project was created for https://github.com/facebook/jest/issues/5740

The following syntax seems to be ignored and broken which results in no tests being found.

See https://facebook.github.io/jest/docs/en/cli.html#jest-regexfortestfiles

```
jest <regexForTestFiles>
```

Previously, this worked just fine with `jest@22.3.0` but I am unable to downgrade
and confirm because of https://github.com/facebook/jest/issues/405 and https://github.com/facebook/jest/issues/3391.
Any tips?

### Workaround

You can update your Jest config to fix the issue.

`jest.config.js`

```js
module.exports = {
  testMatch: [
    '**/test/**/*-test.js?(x)'
  ]
};
```

### Examples

```sh
$ jest --version
v22.4.2
```

```sh
$ npm run jest -- --version

> jest-test-regex1@1.0.0 jest /Users/eric/Documents/javascript/jest-test-regex1
> jest "--version"

v22.4.2
```

#### Relative path to exact test file

```
$ jest ./test/some-test.js
No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: ./test/some-test.js - 0 matches
```

```sh
npm run jest ./test/some-test.js

> jest-test-regex1@1.0.0 jest /Users/eric/Documents/javascript/jest-test-regex1
> jest "./test/some-test.js"

No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: ./test/some-test.js - 0 matches
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! jest-test-regex1@1.0.0 jest: `jest "./test/some-test.js"`
npm ERR! Exit status 1
```

#### Absolute path to exact test file


```sh
$ jest /Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js
No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: /Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js - 0 matches
```

```sh
$ npm run jest /Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js

> jest-test-regex1@1.0.0 jest /Users/eric/Documents/javascript/jest-test-regex1
> jest "/Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js"

No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: /Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js - 0 matches
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! jest-test-regex1@1.0.0 jest: `jest "/Users/eric/Documents/javascript/jest-test-regex1/test/some-test.js"`
npm ERR! Exit status 1
```

#### Glob pattern

```sh
$ jest test/*.js
No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: test/some-test.js - 0 matches
```

```sh
$ npm run jest test/*.js

> jest-test-regex1@1.0.0 jest /Users/eric/Documents/javascript/jest-test-regex1
> jest "test/some-test.js"

No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: test/some-test.js - 0 matches
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! jest-test-regex1@1.0.0 jest: `jest "test/some-test.js"`
npm ERR! Exit status 1
```

#### Glob pattern but passed after `--` with `npm run`

> As of npm@2.0.0, you can use custom arguments when executing scripts. The special option -- is used by getopt to delimit the end of the options. npm will pass all the arguments after the -- directly to your script:
>
> https://docs.npmjs.com/cli/run-script


```sh
$ npm run jest -- test/*.js

> jest-test-regex1@1.0.0 jest /Users/eric/Documents/javascript/jest-test-regex1
> jest "test/some-test.js"

No tests found
In /Users/eric/Documents/javascript/jest-test-regex1
  3 files checked.
  testMatch: **/__tests__/**/*.js?(x),**/?(*.)(spec|test).js?(x) - 0 matches
  testPathIgnorePatterns: /node_modules/ - 3 matches
Pattern: test/some-test.js - 0 matches
npm ERR! code ELIFECYCLE
npm ERR! errno 1
npm ERR! jest-test-regex1@1.0.0 jest: `jest "test/some-test.js"`
npm ERR! Exit status 1
```
